# menu-it

[![](https://badgen.net/packagephobia/install/menu-it)](https://packagephobia.now.sh/result?p=menu-it) [![](https://badgen.net/packagephobia/publish/menu-it)](https://packagephobia.now.sh/result?p=menu-it)

A light weight library allowing you to dynamicaly generate cascading/recursive menus from a starting directory.
Every subfolder of the starting dir can be open as a new menu.
When a .js file is present this one is viewed as an action file and, if selected by the user, is executed.

## Setup

	npm install menu-it

## Usage

`menu-it` exposes a single function; simply pass to this function a path containing your menu tree and it will generate the cascading menu from there.

### Menu tree

Create a directory tree and name your folders as you want them to appear in your menu.
To add action items, just create .js files an name them as you want them to be shown.

Nota : directory and file names will be decamelized and unsnaked when shown in the menu. eg :


*With this directory tree* :
```
├───menu
    ├───git
    │       git_Set-Credentials.js
    │       gitInit.js
    │       gitPull.js
    │       gitPullForce.js
    │       gitPush.js
    │       gitRemoteAdd.js
    │
    ├───setups
            aptUpdate.js
            aptUpgrade.js

```

*you will obtain the following result* :
```
? Select Menu action ...
> Git >
  Setups >
  Back
```

*if you select Git folder* :
```
√ Select Menu action · Git >
? Select Git action ...
> Git Set-Credentials
  Git Init
  Git Pull
  Git Pull Force
  Git Push
  Git Remote Add
  Back
```

### Starting the menu

eg : If your menu starts at path `.menu`, just insert the following require in your code and the menu will run.
```js
require('./lib/menu-it')('./menu');
```

### Action items

An action item file have to export a single function with no input parameters.
This function will be called by menu-it when selected by the user.

*Example of action file* :
```js
const cli = require('../../lib/cli');

async function menuorwhateveryouwant(){
	await cli.exec('git init');
}

module.exports = menuorwhateveryouwant;
```
