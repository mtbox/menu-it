module.exports = {
    extends: 'standard',
		env: {
			browser: true,
	    commonjs: true,
	    es6: true,
	    node: true,
	    jquery: true
	  },
		// add your custom rules here
	  'rules': {
	    'arrow-parens': 0,
			'camelcase': 0,
			'comma-dangle': ['error', 'always-multiline'],
			'generator-star-spacing': 0,
			'global-require': 0,
			'handle-callback-err': 0,
			'indent': ['error', 'tab'],
			'keyword-spacing': ['error', { 'before': true, 'after': true , 'overrides': {
				'if': { 'after': false },
				'for': { 'after': false },
				'while': { 'after': false },
				'switch': { 'after': false }
				} }],
			'no-alert': 0,
			'no-console': ['error', { allow: ['log'] }],
			'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
			'no-multiple-empty-lines': ['error', { 'max': 2, 'maxEOF': 1 }],
			'no-tabs':0,
			'no-underscore-dangle': 0,
			'no-unused-vars': ['error', { 'args': 'none' }],
			'no-useless-escape': 0,
			'padded-blocks': ['error', { 'blocks': 'never' }],
			'prefer-promise-reject-errors': 0,
			'semi': ["error", "always"],
			'space-before-blocks': 0,
			'space-before-function-paren':0,
			'space-infix-ops': 0,
			'import/no-dynamic-require': 0,
			'import/no-extraneous-dependencies': ['error', {'devDependencies': true}],
			'import/no-unresolved': [2, { 'ignore': ['\/~'] }],
			'node/no-deprecated-api': 0
		}

};
