const fs = require('fs');
const path = require('path');
const { Select } = require('enquirer');
let logger = console;

/**
 * Decamelizes a string with/without a custom separator (underscore by default).
 *
 * @param str String in camelcase
 * @param separator Separator for the new decamelized string.
 */
function humanize(str, separator){
	separator = typeof separator === 'undefined' ? '_' : separator;

	return str
		.replace(/_/g, separator)
		.replace(/([a-z\d])([A-Z])/g, '$1' + separator + '$2')
		.replace(/([A-Z]+)([A-Z][a-z\d]+)/g, '$1' + separator + '$2')
		.toLowerCase()
		.replace(/\b\w(?!\b)/g, s => s.toUpperCase());
}

async function run(menupath) {
	menupath=path.resolve(menupath);

	const back = [{ name: 'Back',	path: '', isDir: false }];
	const filelist = fs
		.readdirSync(menupath, 'utf8')
		.map(item => {
			const itempath = path.join(menupath, item);
			const isDir = fs.lstatSync(itempath).isDirectory();
			return {
				name: humanize(`${path.basename(item, '.js')}${isDir ? ' >' : ''}`, ' '),
				path: itempath,
				isDir: isDir,
			};
		})
		.filter(item => {
			return item.isDir || path.extname(item.path)==='.js';
		})
		.sort((a, b) => b.isDir - a.isDir || a.name > b.name ? 1 : -1);

	if(filelist.length) {
		const request = {
			name: menupath,
			message: `Select ${humanize(path.basename(menupath), ' ')} action`,
			choices: filelist.map(item => item.name).concat(back),
		};

		let result;
		/* eslint-disable no-await-in-loop */
		do {
			const prompt = new Select(request);
			result = await prompt.run()
				.catch((err) => logger.error(err));
			// logger.debug('result %s %s', menupath, result);
			const selected = filelist[filelist.findIndex(x => x.name===result)];
			// logger.debug('selected %O', selected);
			if(!selected) {
				return back.name;
			}
			if(selected.isDir) {
				await run(selected.path);
			} else {
				try {
					await require(`${selected.path}`)();
				} catch (err) {
					logger.error('err', err);
					logger.error('module %s not required', selected.path);
				}
			}
		} while(result!==back.name);
		return result;
	}
}

module.exports = run;
